> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
ヤリミールの町ギルド
> CONTEXT: Map097/display_name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]受付
\\C[0]「依頼を探してるの？
　何かあったかな……？ 」
> CONTEXT: Map097/events/1/pages/0/3/Dialogue < UNTRANSLATED
> CONTEXT: Map029/events/2/pages/3/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]受付
\\C[0]「ごめんなさい。うちから出せる依頼は、もうないわ」
> CONTEXT: Map097/events/1/pages/0/8/Dialogue < UNTRANSLATED
> CONTEXT: Map065/events/1/pages/0/8/Dialogue < UNTRANSLATED
> CONTEXT: Map029/events/2/pages/3/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]受付
\\C[0]「報酬に服をくれる依頼が あるといいんだけど…… 」
> CONTEXT: Map097/events/1/pages/0/20/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]受付
\\C[0]「報酬にまともな服をくれる依頼が あるといいんだけど…… 」
> CONTEXT: Map097/events/1/pages/0/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「やっと依頼を引き受けてくれる人が来たか」
> CONTEXT: Map097/events/3/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「実は\\C[2]ヤミリール墓地\\C[0]から夜な夜な\\C[2]幽霊\\C[0]がやってくるんだ。
　こっちから\\C[2]墓地へ行って倒して\\C[0]きてくれないか？ 」
> CONTEXT: Map097/events/3/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「報酬に800Ｇ用意した。よろおしく頼んだよ！ 」
> CONTEXT: Map097/events/3/pages/0/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「\\C[2]ヤミリール墓地\\C[0]へ行って\\C[2]幽霊を倒して\\C[0]きてくれよ」
> CONTEXT: Map097/events/3/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「倒してきてくれたんですね！ これで夜震えずに眠れます！
　どうもありがとう！ 」
> CONTEXT: Map097/events/3/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「さぁ、報酬を受け取って下さい！ 」
> CONTEXT: Map097/events/3/pages/2/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　クエスト４４　達成！
> CONTEXT: Map097/events/3/pages/2/21/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ジェントルメン
\\C[0]「お、ギルドからの人だね、待っていたよ。
　その格好はなんだ……芸術的だな、ふむ」
> CONTEXT: Map097/events/4/pages/0/5/Dialogue < UNTRANSLATED
> CONTEXT: Map097/events/4/pages/0/18/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ジェントルメン
\\C[0]「実は、ワシの大事な\\C[2]犬\\C[0]が居なくなってしまってね」
> CONTEXT: Map097/events/4/pages/0/9/Dialogue < UNTRANSLATED
> CONTEXT: Map097/events/4/pages/0/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ジェントルメン
\\C[0]「お、ギルドからの人だね、待っていたよ。
　ワシの大事な\\C[2]犬\\C[0]が居なくなってしまってね」
> CONTEXT: Map097/events/4/pages/0/30/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ジェントルメン
\\C[0]「おそらく\\C[2]ヤリミール墓地\\C[0]の方へ行ってると
　思ってるんだが、あいにく探しに行く時間もなくてな
　報酬に1200Ｇ用意したから、見つけてきてくれ！ 」
> CONTEXT: Map097/events/4/pages/0/39/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ジェントルメン
\\C[0]「おそらく\\C[2]犬は\\C[0]\\C[2]ヤリミール墓地\\C[0]の方へ行ってると
　思ってるんだが…… 」
> CONTEXT: Map097/events/4/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ジェントルメン
\\C[0]「ポチが帰ってきたよ！ それもすごく機嫌良くね。
　いや～、ありがとう！ 」
> CONTEXT: Map097/events/4/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そ、そうですか…… 」
> CONTEXT: Map097/events/4/pages/2/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ジェントルメン
\\C[0]「さぁ、報酬だ」
> CONTEXT: Map097/events/4/pages/2/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　クエスト４８　達成！
> CONTEXT: Map097/events/4/pages/2/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]女
\\C[0]「\\C[2]おじいちゃん\\C[0]が\\C[2]遺跡\\C[0]の調査に行くとか行って
　しばらく帰ってないんだけど、探してきてくれないかしら？ 」
> CONTEXT: Map097/events/6/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]女
\\C[0]「\\C[2]ヤリミールの南\\C[0]に重要な\\C[2]遺跡\\C[0]が……とか言ってたけど
　そんな物あったかしらねぇ…… 」
> CONTEXT: Map097/events/6/pages/0/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]女
\\C[0]「とにかく、お願いするわね！ 報酬は800Ｇよ」
> CONTEXT: Map097/events/6/pages/0/11/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]女
\\C[0]「\\C[2]おじいちゃん\\C[0]を探してね。\\C[2]ヤリミールの南\\C[0]に重要な\\C[2]遺跡\\C[0]が
　とか言ってたけど…… 」
> CONTEXT: Map097/events/6/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]女
\\C[0]「あぁ、良かった！ おじいちゃん帰ってきました。
　あなたのおかげです。さぁ、報酬をどうぞ」
> CONTEXT: Map097/events/6/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]女
\\C[0]「もう、おじいちゃんたら、たっぷりお仕置きしないと！ 」
> CONTEXT: Map097/events/6/pages/2/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　クエスト６３　達成！
> CONTEXT: Map097/events/6/pages/2/24/Dialogue < UNTRANSLATED

> END STRING
