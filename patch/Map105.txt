> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
オークの村
> CONTEXT: Map105/display_name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そういえば荷物、何かしら？ 」
> CONTEXT: Map105/events/9/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\N[1]へ
お前の人生が幸福になる事を いつも祈っている。
無理はしないように―― ヴィクターより
> CONTEXT: Map105/events/9/pages/0/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃん…… 」
> CONTEXT: Map105/events/9/pages/0/16/Dialogue < UNTRANSLATED
> CONTEXT: Map100/events/3/pages/0/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「ふふ、そろそろお兄様の気持ちが見えてきたかしら？
　でも彼、あなたの前じゃ本音を言わない所もあるわね」
> CONTEXT: Map105/events/9/pages/0/35/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「フェリシアさんには 本音を……？ 」
> CONTEXT: Map105/events/9/pages/0/40/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「あ……違うのよ！ 妬かないでね！ 」
> CONTEXT: Map105/events/9/pages/0/43/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お……お兄ちゃんが そんな事を！？ 」
> CONTEXT: Map105/events/9/pages/1/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「そうよ。私は反対したんだけどね、こじれるだけだって。
　でもね、お兄さんも色々と迷ったり悩んだりしてるの。
　あなたと同じよ」
> CONTEXT: Map105/events/9/pages/1/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そっか…… 」
> CONTEXT: Map105/events/9/pages/1/16/Dialogue < UNTRANSLATED
> CONTEXT: Map010/events/4/pages/0/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「そうそう、今ロイドが 城周辺を探ってくれてるわ。
　彼もすごく必死よ」
> CONTEXT: Map105/events/9/pages/1/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃんのために…… 」
> CONTEXT: Map105/events/9/pages/1/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「やぁね、あなたのために決まってるじゃない！
　彼もあなたを…… 」
> CONTEXT: Map105/events/9/pages/1/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]フェリシア
\\C[0]「助言はここまでにしておくわね。あなたの人生だもの。
　あなた自身で決めなくちゃね」
> CONTEXT: Map105/events/9/pages/1/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「魔物か……倒せば少しは修行になるかな……。
　早くお兄ちゃんを助けて……
　決めなくちゃいけない事もあるわね…… 」
> CONTEXT: Map105/events/11/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「\\N[1]様も色々と 悩んでおられますねぇ」
> CONTEXT: Map105/events/11/pages/0/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうね、案外自分の気持ちってわからないものね」
> CONTEXT: Map105/events/11/pages/0/13/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「モリィめは、\\N[1]様がどのような選択をされようとも
　たとえそれが大失敗であれ、吸血鬼の道に反する事であれ
　いつでも\\N[1]様を応援します！ 」
> CONTEXT: Map105/events/11/pages/0/17/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「モリィ……なんかすごく心強いかも。モリィが
　居てくれるだけで怖くない事、いっぱいあったなぁ」
> CONTEXT: Map105/events/11/pages/0/25/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「お褒め頂き、感激でございますぅ！ 」
> CONTEXT: Map105/events/11/pages/0/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「私一人じゃ、何もできなかったかも……
　モリィって……本当に一生ついてきてくれそうだよね」
> CONTEXT: Map105/events/11/pages/0/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「勿論でございますとも！ どなたとご結婚されようとも
　一生独身を貫こうとも、モリィはっ！ モリィは一生―― 」
> CONTEXT: Map105/events/11/pages/0/37/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「一生独身はないわよ、それならいっそ
　モリィと結婚するもの\\I[122] 」
> CONTEXT: Map105/events/11/pages/0/42/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「そ、そのような思わせぶりな発言は……モリィには
　刺激が強すぎでございますっ！！ 」
> CONTEXT: Map105/events/11/pages/0/47/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうね、ごめんごめん。
　でも私本当に、今は自分が将来どうなってるのか
　想像もつかなくなってきちゃった…… 」
> CONTEXT: Map105/events/11/pages/0/52/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「きっと\\N[1]様は お幸せになりますよ！
　このモリィが保証致します！ 」
> CONTEXT: Map105/events/11/pages/0/58/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうね、なんか勇気が沸いてきた。ありがとうモリィ\\I[122] 」
> CONTEXT: Map105/events/11/pages/0/63/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

モリィの加護を習得した！
> CONTEXT: Map105/events/11/pages/0/71/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「\\N[1]ちゃん！ 」
> CONTEXT: Map105/events/13/pages/0/0/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「ファルサーが\\C[2]城の鍵\\C[0]を部下に預けているらしい……
　そいつが\\C[2]ハゼの崖の頂上\\C[0]から時々 周辺の様子を
　伺ってるみたいなんだ」
> CONTEXT: Map105/events/13/pages/0/26/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そっか、じゃあそこへ行けばそいつから\\C[2]鍵\\C[0]を奪えるわね！ 」
> CONTEXT: Map105/events/13/pages/0/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「……確かにそうだね。君のお兄さんを助けるには
　必要な事だよね。でも…… 」
> CONTEXT: Map105/events/13/pages/0/35/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「どうか怪我はしないで…… 」
> CONTEXT: Map105/events/13/pages/0/45/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「はっ……！ ご、ごめんっ！ そのっ……僕は……架空の
　婚約なんかで君をしばるつもりなんかないんだ……ただ…… 」
> CONTEXT: Map105/events/13/pages/0/52/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「婚約は架空でも……僕の気持ちは本物だよ！ 君の事……っ 」
> CONTEXT: Map105/events/13/pages/0/57/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「………… 」
> CONTEXT: Map105/events/13/pages/0/60/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「今はそんな話をしてる場合じゃないね。さぁ、行って！
　お兄さんを助けなくちゃ！ 」
> CONTEXT: Map105/events/13/pages/0/63/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「これは…… 」
> CONTEXT: Map105/events/13/pages/0/69/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「君に取ってきてもらったアメジストと砂金から作ったんだ。
　少しでも君の役に立つと嬉しいんだけど…… 」
> CONTEXT: Map105/events/13/pages/0/72/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ありがとう…… 」
> CONTEXT: Map105/events/13/pages/0/77/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[2]クエスト５７　城の鍵を入手せよ\\C[0]
発生しました。
> CONTEXT: Map105/events/13/pages/0/87/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]オーク
\\C[0]「この辺にポーション落としたんだが、探してくれないか？ 」
> CONTEXT: Map105/events/14/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「え……？ えぇ…… 」
> CONTEXT: Map105/events/14/pages/0/6/Dialogue < UNTRANSLATED
> CONTEXT: Map060/events/9/pages/0/34/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]オーク
\\C[0]「ほら、ポーション見つけたからやるよ！ 」
> CONTEXT: Map105/events/14/pages/0/23/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
ポーション を貰った！
> CONTEXT: Map105/events/14/pages/0/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]オーク
\\C[0]「そういえば、ポーションは使ったんだった！
　そりゃぁ見つからないよな～ 」
> CONTEXT: Map105/events/14/pages/0/34/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]オーク
\\C[0]「お前、暑がりか？ 」
> CONTEXT: Map105/events/16/pages/0/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]オーク
\\C[0]「お前、寒そう。でもキレイだ…… 」
> CONTEXT: Map105/events/16/pages/0/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]オーク
\\C[0]「オレら、昔はハゼの崖で暮らしてた。でも魔物増えて
　面倒になって村作った。村なら魔物に荒らされない」
> CONTEXT: Map105/events/16/pages/0/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「裸の女性は見慣れています。時々オークがさらっ……
　いえ、何でもないです…… 」
> CONTEXT: Map105/events/17/pages/0/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「人間の町から離れている間に、色々変わったのですね。
　そういう服装が流行ってるのかぁ…… 」
> CONTEXT: Map105/events/17/pages/0/15/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]男
\\C[0]「オークに捕まり、金も女も差し出せなかった私は
　今じゃ使いっぱしりさ。この生活も悪くはないけど…… 」
> CONTEXT: Map105/events/17/pages/0/24/Dialogue < UNTRANSLATED

> END STRING
