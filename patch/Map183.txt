> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
シュロッツー遺跡
> CONTEXT: Map183/display_name/ < UNTRANSLATED
> CONTEXT: Map136/display_name/ < UNTRANSLATED
> CONTEXT: Map135/display_name/ < UNTRANSLATED
> CONTEXT: Map138/events/34/pages/0/2/Dialogue < UNTRANSLATED
> CONTEXT: Map134/display_name/ < UNTRANSLATED
> CONTEXT: Map133/display_name/ < UNTRANSLATED
> CONTEXT: Map132/display_name/ < UNTRANSLATED
> CONTEXT: Map130/display_name/ < UNTRANSLATED
> CONTEXT: Map131/display_name/ < UNTRANSLATED
> CONTEXT: Map129/display_name/ < UNTRANSLATED
> CONTEXT: Map128/display_name/ < UNTRANSLATED
> CONTEXT: Map127/display_name/ < UNTRANSLATED
> CONTEXT: Map126/display_name/ < UNTRANSLATED
> CONTEXT: Map125/display_name/ < UNTRANSLATED
> CONTEXT: Map124/display_name/ < UNTRANSLATED
> CONTEXT: Map123/display_name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
扉は硬く閉ざされている……
> CONTEXT: Map183/events/3/pages/0/1/Dialogue < UNTRANSLATED
> CONTEXT: Map124/events/3/pages/0/1/Dialogue < UNTRANSLATED
> CONTEXT: Map124/events/4/pages/0/1/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/1/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/10/pages/0/1/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/10/pages/1/1/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/10/pages/2/1/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/30/pages/0/1/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/30/pages/1/1/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/30/pages/2/1/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/6/pages/0/1/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/6/pages/1/1/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「開かないわね…… 」
> CONTEXT: Map183/events/3/pages/0/6/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「中にご用ですか？ 」
> CONTEXT: Map183/events/3/pages/0/15/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/15/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「えぇ、人を探しにきたんですけど……
　……あなた誰？ 」
> CONTEXT: Map183/events/3/pages/0/19/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「私はこの遺跡を守る精霊です。中に入るのでしたらこれを…… 」
> CONTEXT: Map183/events/3/pages/0/23/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/23/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「…………何これぇ？ センス悪っ！ 」
> CONTEXT: Map183/events/3/pages/0/33/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「……そのような事を言ってはいけません。それは聖なる
　伝説の鎧――遺跡に入るための正装ですよ」
> CONTEXT: Map183/events/3/pages/0/36/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/36/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……あっそ。まぁいいけど、出たらすぐ脱ぐわよ、こんなの！ 」
> CONTEXT: Map183/events/3/pages/0/41/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/41/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「お似合いですよ。お気をつけて…… 」
> CONTEXT: Map183/events/3/pages/0/44/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/3/pages/0/44/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「ありがとうございました。これで遺跡に平和が戻ります」
> CONTEXT: Map183/events/4/pages/0/24/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/4/pages/3/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「いいのよ。私も自分の場所を乗っ取られて……
　似た者同士ね」
> CONTEXT: Map183/events/4/pages/0/29/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/4/pages/3/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「そうですか……きっとあなたなら、取り戻せますよ」
> CONTEXT: Map183/events/4/pages/0/33/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/4/pages/3/13/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうね、そうするわ」
> CONTEXT: Map183/events/4/pages/0/37/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/4/pages/3/17/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「それでは、ささやかながら、お礼を……」
> CONTEXT: Map183/events/4/pages/0/42/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/4/pages/3/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「いいのよ、そんな気を遣わなくても」
> CONTEXT: Map183/events/4/pages/0/46/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/4/pages/3/26/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「悦びはいくらあっても、多すぎる事はありません…… 」
> CONTEXT: Map183/events/4/pages/0/56/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/4/pages/3/36/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]レミーナー
\\C[0]「これからは、どんな場所でも お好きな格好で歩けますわ\\I[122]
　ふふふ……それではまた…… 」
> CONTEXT: Map183/events/4/pages/0/86/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/4/pages/3/67/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
触手イベントをスキップしますか？
> CONTEXT: Map183/events/8/pages/0/3/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/3/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/3/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/3/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/3/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
はい
> CONTEXT: Map183/events/8/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map170/events/6/pages/0/32/Choice/0 < UNTRANSLATED
> CONTEXT: Map170/events/6/pages/0/44/Choice/0 < UNTRANSLATED
> CONTEXT: Map144/events/3/pages/1/6/Choice/0 < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map106/events/6/pages/0/32/Choice/0 < UNTRANSLATED
> CONTEXT: Map106/events/6/pages/0/45/Choice/0 < UNTRANSLATED
> CONTEXT: Map100/events/9/pages/2/7/Choice/0 < UNTRANSLATED
> CONTEXT: Map094/events/15/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map095/events/5/pages/0/32/Choice/0 < UNTRANSLATED
> CONTEXT: Map095/events/5/pages/0/44/Choice/0 < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map081/events/6/pages/0/32/Choice/0 < UNTRANSLATED
> CONTEXT: Map081/events/6/pages/0/44/Choice/0 < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map064/events/1/pages/0/32/Choice/0 < UNTRANSLATED
> CONTEXT: Map064/events/1/pages/0/44/Choice/0 < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map061/events/3/pages/1/6/Choice/0 < UNTRANSLATED
> CONTEXT: Map061/events/3/pages/2/14/Choice/0 < UNTRANSLATED
> CONTEXT: Map062/events/27/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map061/events/43/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map057/events/42/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map034/events/6/pages/0/3/Choice/0 < UNTRANSLATED
> CONTEXT: Map031/events/3/pages/1/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map031/events/3/pages/1/41/Choice/0 < UNTRANSLATED
> CONTEXT: Map031/events/4/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map030/events/4/pages/3/29/Choice/0 < UNTRANSLATED
> CONTEXT: Map030/events/4/pages/11/28/Choice/0 < UNTRANSLATED
> CONTEXT: Map028/events/30/pages/0/5/Choice/0 < UNTRANSLATED
> CONTEXT: Map005/events/8/pages/0/10/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/2/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/35/pages/0/31/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/36/pages/0/31/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/37/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/38/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/39/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/40/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/41/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/42/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/43/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/44/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/45/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/46/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/47/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/48/pages/0/35/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/49/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/50/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/51/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/52/pages/0/35/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/53/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/54/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/55/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/56/pages/0/35/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/57/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/58/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/59/pages/0/48/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/60/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/70/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/71/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/72/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/73/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/74/pages/0/30/Choice/0 < UNTRANSLATED
> CONTEXT: Map002/events/85/pages/0/11/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/14/3/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/401/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/402/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/403/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/404/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/405/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/406/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/407/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/408/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/409/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/410/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/411/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/412/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/413/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/414/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/415/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/416/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/417/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/425/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/426/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/427/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/428/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/429/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/430/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/431/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/432/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/433/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/434/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/435/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/436/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/437/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/445/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/446/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/447/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/454/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/455/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/456/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/457/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/458/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/459/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/460/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/461/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/462/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/463/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/464/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/471/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/472/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/473/7/Choice/0 < UNTRANSLATED
> CONTEXT: Commonevents/474/7/Choice/0 < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「はぁはぁ……なんだったのかしら……？ 」
> CONTEXT: Map183/events/8/pages/0/15/Dialogue < UNTRANSLATED
> CONTEXT: Map183/events/8/pages/0/67/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/15/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/67/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/15/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/67/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/15/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/67/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/15/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/67/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/15/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/66/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「触手も悪くないわね…………ちょっとクセになりそう……\\I[122] 」
> CONTEXT: Map183/events/8/pages/0/20/Dialogue < UNTRANSLATED
> CONTEXT: Map183/events/8/pages/0/72/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/20/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/72/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/20/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/72/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/20/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/72/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/20/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/72/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/20/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/71/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「はぁ……ビックリしたぁ…… 」
> CONTEXT: Map183/events/8/pages/0/29/Dialogue < UNTRANSLATED
> CONTEXT: Map183/events/8/pages/0/81/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/29/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/81/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/29/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/81/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/29/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/81/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/29/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/81/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/29/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/80/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「でも結構気持ち良かったかも……\\I[122] 」
> CONTEXT: Map183/events/8/pages/0/34/Dialogue < UNTRANSLATED
> CONTEXT: Map183/events/8/pages/0/86/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/34/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/86/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/34/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/86/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/34/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/86/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/34/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/86/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/34/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/85/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……な、なかなかやるじゃない…… 」
> CONTEXT: Map183/events/8/pages/0/43/Dialogue < UNTRANSLATED
> CONTEXT: Map183/events/8/pages/0/95/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/43/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/95/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/43/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/95/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/43/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/95/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/43/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/95/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/43/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/94/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「触手の精液じゃ、お腹いっぱいには
　ならないんだけどね……\\I[122] 」
> CONTEXT: Map183/events/8/pages/0/48/Dialogue < UNTRANSLATED
> CONTEXT: Map183/events/8/pages/0/100/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/48/Dialogue < UNTRANSLATED
> CONTEXT: Map123/events/8/pages/0/100/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/48/Dialogue < UNTRANSLATED
> CONTEXT: Map105/events/15/pages/0/100/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/48/Dialogue < UNTRANSLATED
> CONTEXT: Map087/events/6/pages/0/100/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/48/Dialogue < UNTRANSLATED
> CONTEXT: Map072/events/3/pages/0/100/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/48/Dialogue < UNTRANSLATED
> CONTEXT: Map062/events/22/pages/0/99/Dialogue < UNTRANSLATED

> END STRING
