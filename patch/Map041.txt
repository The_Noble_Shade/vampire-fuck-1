> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
\\C[1]ファルサー
\\C[0]「\\N[1]様の様子が気になって、個人的に見にきました。
　これからエルフの救出に向かわれるのですね」
> CONTEXT: Map041/events/3/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうよ！ 困ってるエルフさんを
　助けてあげるんだから！ 」
> CONTEXT: Map041/events/3/pages/0/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「はっ！！ 吸血鬼のセリフとは思えませんな」
> CONTEXT: Map041/events/3/pages/0/18/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「どういう意味よ？ 」
> CONTEXT: Map041/events/3/pages/0/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「エルフというのは本来、吸血鬼にとっては奴隷であり
　家畜のような存在。腹が減れば血を吸い、暇つぶしに
　犯す為の肉壺。それを助けてやろうなどとは…… 」
> CONTEXT: Map041/events/3/pages/0/25/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「なにそれ？ 感じ悪～い」
> CONTEXT: Map041/events/3/pages/0/31/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「\\N[1]様は血も吸わぬ身、それに温室育ちゆえ
　吸血鬼の暗部を知らないようですな」
> CONTEXT: Map041/events/3/pages/0/34/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「兄上様も吸血鬼ですからな、色んな意味でエルフどもを
　利用してきたのですぞ」
> CONTEXT: Map041/events/3/pages/0/38/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……そ、そうなの……？ そんなの聞いた事ないけど…… 」
> CONTEXT: Map041/events/3/pages/0/43/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「かわいい妹君に、そのようなお姿を見せたく
　なかったのでしょうな」
> CONTEXT: Map041/events/3/pages/0/46/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お、お兄ちゃんはそんな事しないもん…… 」
> CONTEXT: Map041/events/3/pages/0/51/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「私も 見た事も聞いた事もありません！ 」
> CONTEXT: Map041/events/3/pages/0/61/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「おやおや、余計な事を言ってしまったようですな。
　失礼しました。ファルサーめは、\\N[1]様が
　心配で来ただけでしたのに……どうか頑張って下さい」
> CONTEXT: Map041/events/3/pages/0/65/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「\\N[1]様……ファルサーの言う事など、気にしては
　いけませんよ」
> CONTEXT: Map041/events/3/pages/0/76/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……そうね、今は自分のやるべき事に集中しなくちゃ」
> CONTEXT: Map041/events/3/pages/0/83/Dialogue < UNTRANSLATED

> END STRING
