> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ここが例の墓地ね！ どこかに地下道への入り口があるはず…… 」
> CONTEXT: Map018/events/1/pages/0/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「新しい人生への第一歩ですね、おめでとうございます」
> CONTEXT: Map018/events/1/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「でもココお墓でしょ？ ちょっと不気味～」
> CONTEXT: Map018/events/1/pages/0/13/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「吸血鬼であられる\\N[1]様が、お墓を怖がりますとは
　なんとも…… 」
> CONTEXT: Map018/events/1/pages/0/17/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「べっ、別に怖くなんかないわよ！ 」
> CONTEXT: Map018/events/1/pages/0/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「人間から見た吸血鬼も、恐怖の対象であるかと……
　人間に会う時には、正体がバレぬよう心がけた方がよろしいかと」
> CONTEXT: Map018/events/1/pages/0/26/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうね。さっさとココを抜けて町に行きましょう！
　私、人間の町って初めて\\I[122] 楽しみだなっ」
> CONTEXT: Map018/events/1/pages/0/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「あの…… 」
> CONTEXT: Map018/events/3/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「キャー！！！ やだぁ、何！？ キャー！！  」
> CONTEXT: Map018/events/3/pages/0/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「ちょ……ちょっと、そんなに騒がないで下さいよ。
　うるさいなぁ、まったく」
> CONTEXT: Map018/events/3/pages/0/11/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ご、ごめん……驚いちゃって…… 」
> CONTEXT: Map018/events/3/pages/0/16/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「私の\\C[2]頭\\C[0]、知りませんか……？
　どこかに置き忘れてきたみたいなんです…… 」
> CONTEXT: Map018/events/3/pages/0/21/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「う～ん……見てないけど…… 」
> CONTEXT: Map018/events/3/pages/0/26/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「ちょっと探してきてもらえません……？ 」
> CONTEXT: Map018/events/3/pages/0/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「え……私が？ 」
> CONTEXT: Map018/events/3/pages/0/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「アンタ、\\C[2]頭\\C[0]がついてるだけ幸せでしょ～。それくらい
　やってくれてもいいと思うんだよね。頼んだよ！ 」
> CONTEXT: Map018/events/3/pages/0/36/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「わかったわ…… 」
> CONTEXT: Map018/events/3/pages/0/41/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[2]クエスト５　私の頭を…\\C[0]
受注しました。
> CONTEXT: Map018/events/3/pages/0/47/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「石が見える……たぶん墓石の側にあると思うんだよね～ 」
> CONTEXT: Map018/events/3/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「墓石って……ここ、墓石だらけじゃないの！ 」
> CONTEXT: Map018/events/3/pages/1/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「花……花も見えるよ、黄色い花だ 」
> CONTEXT: Map018/events/3/pages/1/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お花か…… 」
> CONTEXT: Map018/events/3/pages/1/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「はい、これ！ 持ってきたわ！ 」
> CONTEXT: Map018/events/3/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「おぉ、ありがとう！ 久しぶりだな、我が頭よ！ 」
> CONTEXT: Map018/events/3/pages/2/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「ふぅ、これでいい…… 」
> CONTEXT: Map018/events/3/pages/2/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「ありがとうよ！ お礼にコレをやろう！ 」
> CONTEXT: Map018/events/3/pages/2/21/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あら、ありがとう」
> CONTEXT: Map018/events/3/pages/2/26/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「その辺で拾ったけど、私が持ってても仕方ないしね」
> CONTEXT: Map018/events/3/pages/2/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　　クエスト５　達成！
> CONTEXT: Map018/events/3/pages/2/34/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]うっかり屋のゾンビ
\\C[0]「しかし私は、ここで何をしてたんだっけ？
　物忘れが激しくて困っちゃうよ」
> CONTEXT: Map018/events/3/pages/3/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そ、そう…… 」
> CONTEXT: Map018/events/3/pages/3/7/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/1/pages/3/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ゾンビの頭
\\C[0]「アンタ……ボディと話した娘だろ？ さぁ、オイラを
　ボディの所に届けておくれ！ 」
> CONTEXT: Map018/events/4/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「え、えぇ……わかってるわ。運べばいいのね…… 」
> CONTEXT: Map018/events/4/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ゾンビの頭
\\C[0]「アンタ、『 いやぁん、気持ち悪い！ やだぁ！ 』とか
　思ってるだろ！？ 」
> CONTEXT: Map018/events/4/pages/0/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そそそ、そんな事ないわよ！ 運ぶわよ、運ぶわ！ 」
> CONTEXT: Map018/events/4/pages/0/17/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ゾンビの頭
\\C[0]「さぁ！ ずずいと！ 」
> CONTEXT: Map018/events/4/pages/0/20/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「はいはい…… 」
> CONTEXT: Map018/events/4/pages/0/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
セントヴァーギナ墓地
一族の眠る墓
> CONTEXT: Map018/events/20/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「吸血鬼も死ぬのかしら……？ 」
> CONTEXT: Map018/events/20/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　  ……マ…コン卿
　　　　　　　　　　　　ここに眠る
　　　　　　　　　 死因　ビーチで日光浴
> CONTEXT: Map018/events/46/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「吸血鬼も 死ぬ時があるのね…… 」
> CONTEXT: Map018/events/46/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　  ……テ…ガン伯爵
　　　　　　　　　　　　ここに眠る
　　　　　　　　　 死因　シルバーピアス
> CONTEXT: Map018/events/47/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……お洒落さんかしら？ 」
> CONTEXT: Map018/events/47/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　 ……パ……ツン卿
　　　　　　　　　　　　ここに眠る
　　　　　　　　　 死因　ペペロンチーノ
> CONTEXT: Map018/events/48/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……？ 」
> CONTEXT: Map018/events/48/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　  ……イ…ブバ伯爵
　　　　　　　　　　　　ここに眠る
　　　　　　　　　   死因　銀食器で食事
> CONTEXT: Map018/events/49/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……吸血鬼の一族って……大丈夫なのかしら？ 」
> CONTEXT: Map018/events/49/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　  ……タ…ーロ伯爵
　　　　　　　　　　　　ここに眠る
　　　　　　　　　 死因　剣士に討伐される
> CONTEXT: Map018/events/50/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ま、まともな死因もあるじゃない！ 」
> CONTEXT: Map018/events/50/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　  ……マ…スタ卿
　　　　　　　　　　　　ここに眠る
　　　　　　　　　 死因　血液アレルギー
> CONTEXT: Map018/events/51/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　  ……ン…デマ公爵
　　　　　　　　　　　　ここに眠る
　　　　　　　　　 死因　男女関係のもつれ
> CONTEXT: Map018/events/52/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「いちいち死因まで書かないとダメなのかしら……？ 」
> CONTEXT: Map018/events/52/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　  ……ナ…ルア男爵
　　　　　　　　　　　　ここに眠る
　　　　　　　　　 死因　銀ナイフによる自傷
> CONTEXT: Map018/events/53/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「色々あるのね………… 」
> CONTEXT: Map018/events/53/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
　　　　　　　　　　  ……ン…コチ卿
　　　　　　　　　　　　ここに眠る
　　　　　　　　　死因　無農薬ニンニク栽培
> CONTEXT: Map018/events/54/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「…………？？ 」
> CONTEXT: Map018/events/54/pages/0/9/Dialogue < UNTRANSLATED

> END STRING
