> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
ハートウィップ
> CONTEXT: Weapons/1/name/ < UNTRANSLATED
Heart Whip
> END STRING

> BEGIN STRING
ハート型のかわいい鞭。
> CONTEXT: Weapons/1/description/ < UNTRANSLATED
A cute heart-shaped whip.
> END STRING

> BEGIN STRING
ロングウィップ
> CONTEXT: Weapons/2/name/ < UNTRANSLATED
Long Whip
> END STRING

> BEGIN STRING
長めの一本鞭。
> CONTEXT: Weapons/2/description/ < UNTRANSLATED
A moderately long whip.
> END STRING

> BEGIN STRING
バラ鞭ロング
> CONTEXT: Weapons/3/name/ < UNTRANSLATED
Long Rose Whip
> END STRING

> BEGIN STRING
長めのバラ鞭。
> CONTEXT: Weapons/3/description/ < UNTRANSLATED
A moderately long Rose Whip.
> END STRING

> BEGIN STRING
バラ鞭ショート
> CONTEXT: Weapons/4/name/ < UNTRANSLATED
Short Rose Whip
> END STRING

> BEGIN STRING
短めのバラ鞭。
> CONTEXT: Weapons/4/description/ < UNTRANSLATED
A rather short Rose Whip.
> END STRING

> BEGIN STRING
羽根箒
> CONTEXT: Weapons/6/name/ < UNTRANSLATED
Feather Broom
> END STRING

> BEGIN STRING
フワフワの羽。
> CONTEXT: Weapons/6/description/ < UNTRANSLATED
Made with fluffy feathers.
> END STRING

> BEGIN STRING
コスメブラシ
> CONTEXT: Weapons/7/name/ < UNTRANSLATED
Cosmetic Brush
> END STRING

> BEGIN STRING
フカフカの毛がついた刷毛。チーク用の大きめサイズ。
> CONTEXT: Weapons/7/description/ < UNTRANSLATED
Fluffy brush. Large sized, easier to apply blush with it.
> END STRING

> BEGIN STRING
ハンドアクス
> CONTEXT: Weapons/100/name/ < UNTRANSLATED
Hand Axe
> END STRING

> BEGIN STRING
伐採に使う小型の斧。
> CONTEXT: Weapons/100/description/ < UNTRANSLATED
Small axe used for logging.
> END STRING

> BEGIN STRING
バトルアクス
> CONTEXT: Weapons/101/name/ < UNTRANSLATED
Battle Axe
> END STRING

> BEGIN STRING
戦闘用に作られた両刃の斧。
> CONTEXT: Weapons/101/description/ < UNTRANSLATED
Double edged axe designed for battle.
> END STRING

> BEGIN STRING
バルディッシュ
> CONTEXT: Weapons/102/name/ < UNTRANSLATED
Bardiche
> END STRING

> BEGIN STRING
三日月型の刃をもつ戦斧。
> CONTEXT: Weapons/102/description/ < UNTRANSLATED
Battle axe with a crescent-shaped blade.
> END STRING

> BEGIN STRING
ミスリルアクス
> CONTEXT: Weapons/103/name/ < UNTRANSLATED
Mithril Axe
> END STRING

> BEGIN STRING
魔法の金属ミスリルで作られた斧。
> CONTEXT: Weapons/103/description/ < UNTRANSLATED
Axe made from the magic metal mithril.
> END STRING

> BEGIN STRING
クリムゾンアクス
> CONTEXT: Weapons/104/name/ < UNTRANSLATED
Crimson Axe
> END STRING

> BEGIN STRING
真紅に染め上げられた戦斧。
> CONTEXT: Weapons/104/description/ < UNTRANSLATED
A battle axe dyed intense crimson.
> END STRING

> BEGIN STRING
魔斧ギガンテス
> CONTEXT: Weapons/105/name/ < UNTRANSLATED
Giant's Axe
> END STRING

> BEGIN STRING
巨人族の英雄が使ったとされる重斧。
> CONTEXT: Weapons/105/description/ < UNTRANSLATED
A heavy axe used by the hero of the giants.
> END STRING

> BEGIN STRING
セスタス
> CONTEXT: Weapons/106/name/ < UNTRANSLATED
Cestus
> END STRING

> BEGIN STRING
拳頭にトゲがついたグローブ。
> CONTEXT: Weapons/106/description/ < UNTRANSLATED
A glove equipped with spikes on the knuckles.
> END STRING

> BEGIN STRING
バグナウ
> CONTEXT: Weapons/107/name/ < UNTRANSLATED
Bagh Nagh
> END STRING

> BEGIN STRING
短い４本の爪が取りつけられた格闘武器。
> CONTEXT: Weapons/107/description/ < UNTRANSLATED
Fighting weapon with four small claws attached to it.
> END STRING

> BEGIN STRING
アイアンクロウ
> CONTEXT: Weapons/108/name/ < UNTRANSLATED
Iron Claw
> END STRING

> BEGIN STRING
鋼鉄のかぎ爪が仕込まれた手甲。
> CONTEXT: Weapons/108/description/ < UNTRANSLATED
Glove-like weapon with sharp iron blades attacked to it.
> END STRING

> BEGIN STRING
ミスリルクロウ
> CONTEXT: Weapons/109/name/ < UNTRANSLATED
Mithril Claw
> END STRING

> BEGIN STRING
ミスリルのかぎ爪が仕込まれた手甲。
> CONTEXT: Weapons/109/description/ < UNTRANSLATED
Glove-like weapon with sharp mithril blades attacked to it.
> END STRING

> BEGIN STRING
ドラゴンファング
> CONTEXT: Weapons/110/name/ < UNTRANSLATED
Dragon Fang
> END STRING

> BEGIN STRING
竜牙の拳頭をもつ手甲。
> CONTEXT: Weapons/110/description/ < UNTRANSLATED
Glove-like weapon with sharp dragon fangs attacked to it.
> END STRING

> BEGIN STRING
魔爪ゼファー
> CONTEXT: Weapons/111/name/ < UNTRANSLATED
Zephyr Claws
> END STRING

> BEGIN STRING
西風の名を冠する魔法の爪。
> CONTEXT: Weapons/111/description/ < UNTRANSLATED
Magical claw named after the west wind.
> END STRING

> BEGIN STRING
スピア
> CONTEXT: Weapons/112/name/ < UNTRANSLATED
Spear
> END STRING

> BEGIN STRING
突きに特化させた短めの槍。
> CONTEXT: Weapons/112/description/ < UNTRANSLATED
A short spear designed for stabbing.
> END STRING

> BEGIN STRING
パルチザン
> CONTEXT: Weapons/113/name/ < UNTRANSLATED
Partisan
> END STRING

> BEGIN STRING
幅広の刃をもった鋼鉄製の長槍。
> CONTEXT: Weapons/113/description/ < UNTRANSLATED
Long steel spear with a wide blade.
> END STRING

> BEGIN STRING
ハルバード
> CONTEXT: Weapons/114/name/ < UNTRANSLATED
Halberd
> END STRING

> BEGIN STRING
穂先に斧頭の取りつけられた槍。
> CONTEXT: Weapons/114/description/ < UNTRANSLATED
Spear with an axe head attached.
> END STRING

> BEGIN STRING
ミスリルスピア
> CONTEXT: Weapons/115/name/ < UNTRANSLATED
Mithril Spear
> END STRING

> BEGIN STRING
魔法の金属ミスリルで作られた長槍。
> CONTEXT: Weapons/115/description/ < UNTRANSLATED
Long spear made from the magical metal mithril.
> END STRING

> BEGIN STRING
ホーリーランス
> CONTEXT: Weapons/116/name/ < UNTRANSLATED
Holy Lance
> END STRING

> BEGIN STRING
白き神の祝福を受けた聖なる槍。
> CONTEXT: Weapons/116/description/ < UNTRANSLATED
Holy lance blessed with the power of a god.
> END STRING

> BEGIN STRING
魔槍ゲイボルグ
> CONTEXT: Weapons/117/name/ < UNTRANSLATED
Gaebolg
> END STRING

> BEGIN STRING
影の国に伝わる魔力を帯びた槍。
> CONTEXT: Weapons/117/description/ < UNTRANSLATED
Magical spear passed down in the shadow realm.
> END STRING

> BEGIN STRING
ショートソード
> CONTEXT: Weapons/118/name/ < UNTRANSLATED
Short Sword
> END STRING

> BEGIN STRING
軽くて扱いやすい刀身の短い剣。
> CONTEXT: Weapons/118/description/ < UNTRANSLATED
Light and easy to wield sword.
> END STRING

> BEGIN STRING
ロングソード
> CONTEXT: Weapons/119/name/ < UNTRANSLATED
> CONTEXT: Imrecipes/17/name/ < UNTRANSLATED
Long Sword
> END STRING

> BEGIN STRING
鋭い切れ味を誇る戦闘用の長剣。
> CONTEXT: Weapons/119/description/ < UNTRANSLATED
A very sharp long sword used for battle.
> END STRING

> BEGIN STRING
ファルシオン
> CONTEXT: Weapons/120/name/ < UNTRANSLATED
> CONTEXT: Imrecipes/18/name/ < UNTRANSLATED
Falchion
> END STRING

> BEGIN STRING
反りのある刀身をもつ幅広の剣。
> CONTEXT: Weapons/120/description/ < UNTRANSLATED
Broad sword with a curved blade.
> END STRING

> BEGIN STRING
ミスリルソード
> CONTEXT: Weapons/121/name/ < UNTRANSLATED
Mithril Sword
> END STRING

> BEGIN STRING
魔法の金属ミスリルで作られた長剣。
> CONTEXT: Weapons/121/description/ < UNTRANSLATED
Long sword crafted from the magical metal mithril.
> END STRING

> BEGIN STRING
ルーンブレード
> CONTEXT: Weapons/122/name/ < UNTRANSLATED
Rune Blade
> END STRING

> BEGIN STRING
刀身にルーン文字の刻まれた剣。
> CONTEXT: Weapons/122/description/ < UNTRANSLATED
Sword engraved with magical runes.
> END STRING

> BEGIN STRING
魔剣ティルヴィング
> CONTEXT: Weapons/123/name/ < UNTRANSLATED
Tryfing
> END STRING

> BEGIN STRING
勝利と災厄をもたらす魔剣。
> CONTEXT: Weapons/123/description/ < UNTRANSLATED
Magical sword that brings with it both victory and disaster.
> END STRING

> BEGIN STRING
無銘の刀
> CONTEXT: Weapons/124/name/ < UNTRANSLATED
Unnamed Katana
> END STRING

> BEGIN STRING
銘をもたない細身の刀。
> CONTEXT: Weapons/124/description/ < UNTRANSLATED
Thin-bladed katana with no engravings on it.
> END STRING

> BEGIN STRING
斬馬刀
> CONTEXT: Weapons/125/name/ < UNTRANSLATED
Zanbato
> END STRING

> BEGIN STRING
破壊力に優れた太刀。
> CONTEXT: Weapons/125/description/ < UNTRANSLATED
A katana that excels in destructive power.
> END STRING

> BEGIN STRING
虎徹
> CONTEXT: Weapons/126/name/ < UNTRANSLATED
Kotetsu
> END STRING

> BEGIN STRING
名のある刀匠に鍛えられた業物の刀。
> CONTEXT: Weapons/126/description/ < UNTRANSLATED
A katana crafted by a famous swordsmith.
> END STRING

> BEGIN STRING
霊銀の太刀
> CONTEXT: Weapons/127/name/ < UNTRANSLATED
Silver Soul Katana
> END STRING

> BEGIN STRING
魔法の金属で作られた太刀。
> CONTEXT: Weapons/127/description/ < UNTRANSLATED
Katana crafted from magical metal.
> END STRING

> BEGIN STRING
七支刀
> CONTEXT: Weapons/128/name/ < UNTRANSLATED
Seven Branched Katana
> END STRING

> BEGIN STRING
破邪の力を秘めた古代刀。
> CONTEXT: Weapons/128/description/ < UNTRANSLATED
An ancient sword possessing secret powers.
> END STRING

> BEGIN STRING
妖刀ムラマサ
> CONTEXT: Weapons/129/name/ < UNTRANSLATED
Muramasa
> END STRING

> BEGIN STRING
剣士たちの生き血を吸ってきた魔性の刀。
> CONTEXT: Weapons/129/description/ < UNTRANSLATED
Magic katana that feeds on the blood of its opponents.
> END STRING

> BEGIN STRING
ショートボウ
> CONTEXT: Weapons/130/name/ < UNTRANSLATED
Short Bow
> END STRING

> BEGIN STRING
狩猟用に作られた短めの弓。
> CONTEXT: Weapons/130/description/ < UNTRANSLATED
Short bow made for hunting.
> END STRING

> BEGIN STRING
ロングボウ
> CONTEXT: Weapons/131/name/ < UNTRANSLATED
Longbow
> END STRING

> BEGIN STRING
威力を高めた戦闘用の長弓。
> CONTEXT: Weapons/131/description/ < UNTRANSLATED
Bow made for battle with increased fire power.
> END STRING

> BEGIN STRING
クロスボウ
> CONTEXT: Weapons/132/name/ < UNTRANSLATED
> CONTEXT: Imrecipes/19/name/ < UNTRANSLATED
Crossbow
> END STRING

> BEGIN STRING
強力なバネで矢を射出する石弓。
> CONTEXT: Weapons/132/description/ < UNTRANSLATED
Powerful crossbow that fires arrows using a spring mechanism.
> END STRING

> BEGIN STRING
ミスリルボウ
> CONTEXT: Weapons/133/name/ < UNTRANSLATED
Mithril Bow
> END STRING

> BEGIN STRING
魔法の金属ミスリルで作られた弓。
> CONTEXT: Weapons/133/description/ < UNTRANSLATED
Bow crafted from the magical metal mithril.
> END STRING

> BEGIN STRING
エルブンボウ
> CONTEXT: Weapons/134/name/ < UNTRANSLATED
Elven Bow
> END STRING

> BEGIN STRING
妖精の森に生える霊木から作られた弓。
> CONTEXT: Weapons/134/description/ < UNTRANSLATED
Bow crafted from a spirit tree from the forest of faeries.
> END STRING

> BEGIN STRING
魔弓アルテミス
> CONTEXT: Weapons/135/name/ < UNTRANSLATED
Artemis Bow
> END STRING

> BEGIN STRING
月の女神の名を冠する白銀の弓。
> CONTEXT: Weapons/135/description/ < UNTRANSLATED
Silver bow bearing the name of the goddess of hunting.
> END STRING

> BEGIN STRING
ナイフ
> CONTEXT: Weapons/136/name/ < UNTRANSLATED
Knife
> END STRING

> BEGIN STRING
日常生活にも使えるナイフ。
> CONTEXT: Weapons/136/description/ < UNTRANSLATED
Knife that can also be used in everyday life.
> END STRING

> BEGIN STRING
ダガー
> CONTEXT: Weapons/137/name/ < UNTRANSLATED
Dagger
> END STRING

> BEGIN STRING
幅広の刀身をもつ戦闘用の短刀。
> CONTEXT: Weapons/137/description/ < UNTRANSLATED
Battle knife with a broad blade.
> END STRING

> BEGIN STRING
マインゴーシュ
> CONTEXT: Weapons/138/name/ < UNTRANSLATED
Main Gauche
> END STRING

> BEGIN STRING
攻撃を受け流すこともできる短剣。
> CONTEXT: Weapons/138/description/ < UNTRANSLATED
Dagger that can also help evade attacks.
> END STRING

> BEGIN STRING
ミスリルナイフ
> CONTEXT: Weapons/139/name/ < UNTRANSLATED
Mithril Knife
> END STRING

> BEGIN STRING
魔法の金属ミスリルで作られたナイフ。
> CONTEXT: Weapons/139/description/ < UNTRANSLATED
Knife crafted from the magic metal mithril.
> END STRING

> BEGIN STRING
アサシンダガー
> CONTEXT: Weapons/140/name/ < UNTRANSLATED
Assassin's Dagger
> END STRING

> BEGIN STRING
暗殺者が好んで使う漆黒の短刀。
> CONTEXT: Weapons/140/description/ < UNTRANSLATED
Jet black dagger preferred by assassins.
> END STRING

> BEGIN STRING
魔刃ヴォーテクス
> CONTEXT: Weapons/141/name/ < UNTRANSLATED
Vortex
> END STRING

> BEGIN STRING
竜巻の名を冠する魔法の短剣。
> CONTEXT: Weapons/141/description/ < UNTRANSLATED
Dagger bearing the name of a tornado.
> END STRING

> BEGIN STRING
メイス
> CONTEXT: Weapons/142/name/ < UNTRANSLATED
Mace
> END STRING

> BEGIN STRING
打撃力を高めた金属製の棍棒。
> CONTEXT: Weapons/142/description/ < UNTRANSLATED
Metal club with increased striking power.
> END STRING

> BEGIN STRING
フレイル
> CONTEXT: Weapons/143/name/ < UNTRANSLATED
Flail
> END STRING

> BEGIN STRING
柄の先に鎖で鉄球をつないだ打撃武器。
> CONTEXT: Weapons/143/description/ < UNTRANSLATED
Battle weapon consisting of a ball and chain attached to a handle.
> END STRING

> BEGIN STRING
ウォーハンマー
> CONTEXT: Weapons/144/name/ < UNTRANSLATED
Warhammer
> END STRING

> BEGIN STRING
戦闘用に作られた鋼鉄のハンマー。
> CONTEXT: Weapons/144/description/ < UNTRANSLATED
Steel hammer designed for battle.
> END STRING

> BEGIN STRING
ミスリルメイス
> CONTEXT: Weapons/145/name/ < UNTRANSLATED
Mithril Mace
> END STRING

> BEGIN STRING
魔法の金属ミスリルで作られた戦槌。
> CONTEXT: Weapons/145/description/ < UNTRANSLATED
Mace crafted from the magical metal mithril.
> END STRING

> BEGIN STRING
アースブレイカー
> CONTEXT: Weapons/146/name/ < UNTRANSLATED
Earthbeaker
> END STRING

> BEGIN STRING
大地の力を秘めた魔法のハンマー。
> CONTEXT: Weapons/146/description/ < UNTRANSLATED
Magical hammer infused with the power of the earth.
> END STRING

> BEGIN STRING
魔槌ミョルニル
> CONTEXT: Weapons/147/name/ < UNTRANSLATED
Mjölnir
> END STRING

> BEGIN STRING
“打ち砕くもの”の異名をもつ戦神の槌。
> CONTEXT: Weapons/147/description/ < UNTRANSLATED
War hammer also know as the “Smasher”
> END STRING

> BEGIN STRING
ウッドスタッフ
> CONTEXT: Weapons/148/name/ < UNTRANSLATED
Wood Staff
> END STRING

> BEGIN STRING
樫の木で作られた堅い杖。
> CONTEXT: Weapons/148/description/ < UNTRANSLATED
Sturdy staff made out of oak wood.
> END STRING

> BEGIN STRING
魔道士の杖
> CONTEXT: Weapons/149/name/ < UNTRANSLATED
Mage Staff
> END STRING

> BEGIN STRING
精神集中を助ける宝珠のついた杖。
> CONTEXT: Weapons/149/description/ < UNTRANSLATED
A staff that helps spiritual concentration.
> END STRING

> BEGIN STRING
フォースワンド
> CONTEXT: Weapons/150/name/ < UNTRANSLATED
Force Staff
> END STRING

> BEGIN STRING
術者の魔力を増幅させる銀製の杖。
> CONTEXT: Weapons/150/description/ < UNTRANSLATED
Silver staff designed to amplify the power of the caster.
> END STRING

> BEGIN STRING
ミスリルロッド
> CONTEXT: Weapons/151/name/ < UNTRANSLATED
Mithril Rod
> END STRING

> BEGIN STRING
魔法の金属ミスリルで作られた杖。
> CONTEXT: Weapons/151/description/ < UNTRANSLATED
Rod made out of the magical metal mithril.
> END STRING

> BEGIN STRING
霊木の杖
> CONTEXT: Weapons/152/name/ < UNTRANSLATED
Sacred Staff
> END STRING

> BEGIN STRING
ヤドリギの老木から作られた杖。
> CONTEXT: Weapons/152/description/ < UNTRANSLATED
Staff made from an ancient mistletoe.
> END STRING

> BEGIN STRING
魔杖アゾット
> CONTEXT: Weapons/153/name/ < UNTRANSLATED
Azoth Staff
> END STRING

> BEGIN STRING
賢者の石が埋め込まれた魔杖。
> CONTEXT: Weapons/153/description/ < UNTRANSLATED
Staff adorned with a magical gem of wisdom.
> END STRING

> BEGIN STRING
フリントロック
> CONTEXT: Weapons/154/name/ < UNTRANSLATED
Flintlock
> END STRING

> BEGIN STRING
火打ち式の旧式拳銃。
> CONTEXT: Weapons/154/description/ < UNTRANSLATED
Handgun with an old-fashioned firing mechanism.
> END STRING

> BEGIN STRING
マスケット
> CONTEXT: Weapons/155/name/ < UNTRANSLATED
Musket
> END STRING

> BEGIN STRING
銃士に支給される歩兵銃。
> CONTEXT: Weapons/155/description/ < UNTRANSLATED
Rifle used by musketeer infantry.
> END STRING

> BEGIN STRING
ドラグーン
> CONTEXT: Weapons/156/name/ < UNTRANSLATED
Dragoon
> END STRING

> BEGIN STRING
騎兵が使用するカービン銃。
> CONTEXT: Weapons/156/description/ < UNTRANSLATED
A carbine used by the cavalry.
> END STRING

> BEGIN STRING
霊銀銃
> CONTEXT: Weapons/157/name/ < UNTRANSLATED
Demon Silver Gun
> END STRING

> BEGIN STRING
魔法の金属ミスリルの銃身をもつ銃。
> CONTEXT: Weapons/157/description/ < UNTRANSLATED
Gun made out of magical metal mithril.
> END STRING

> BEGIN STRING
ピースメイカー
> CONTEXT: Weapons/158/name/ < UNTRANSLATED
Peacemaker
> END STRING

> BEGIN STRING
黒色火薬を用いたリボルバー式拳銃。
> CONTEXT: Weapons/158/description/ < UNTRANSLATED
Black revolver that uses gunpowder.
> END STRING

> BEGIN STRING
魔銃エーテルブラスト
> CONTEXT: Weapons/159/name/ < UNTRANSLATED
Ether Blaster
> END STRING

> BEGIN STRING
霊子を弾丸に換えて撃ち出す魔銃。
> CONTEXT: Weapons/159/description/ < UNTRANSLATED
An evil gun that fires off bullets infused with the souls of the departed.
> END STRING
