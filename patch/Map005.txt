> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
トランシルヴァーギナ城 １階
> CONTEXT: Map005/display_name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「やぁ、\\N[1]ちゃん。元気かい？ 」
> CONTEXT: Map005/events/1/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ここで何してるの？ 」
> CONTEXT: Map005/events/1/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「実はね、君にこれを…… 」
> CONTEXT: Map005/events/1/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「私の好きな色\\I[122] 覚えててくれたのね。
　ありがとうロイド！ 」
> CONTEXT: Map005/events/1/pages/0/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「君の事は小さい頃から知ってるからね、お兄さん程じゃないのが
　残念だけど…… 」
> CONTEXT: Map005/events/1/pages/0/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃんなんか……私の事全然わかってないわよ！ 」
> CONTEXT: Map005/events/1/pages/0/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「そうかな…… 」
> CONTEXT: Map005/events/1/pages/0/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「\\N[1]ちゃん、最近元気ないね」
> CONTEXT: Map005/events/1/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「う～ん……色々とね…… 」
> CONTEXT: Map005/events/1/pages/1/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「\\N[1]ちゃんは笑顔でいるのが一番似合ってるよ。
　悩みがあるなら聞くよ！ 」
> CONTEXT: Map005/events/1/pages/1/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「平気よ！ これでも私、結構強いんだからっ 」
> CONTEXT: Map005/events/1/pages/1/16/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「そっか…… 」
> CONTEXT: Map005/events/1/pages/1/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「僕じゃダメかな……頑張って君を振り向かせるよ！ 」
> CONTEXT: Map005/events/1/pages/3/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「\\N[1]ちゃん、昔から可愛かったよね。
　僕の初恋だって……知ってた？ 」
> CONTEXT: Map005/events/1/pages/4/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃんに言われて、私と婚約したんじゃないの？ 」
> CONTEXT: Map005/events/1/pages/4/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「お兄さんは僕の気持ちに気づいてたからね。僕にとって
 婚約は嬉しい事だったけど、\\N[1]ちゃんは…… 」
> CONTEXT: Map005/events/1/pages/4/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「べ、別にロイドの事嫌いなワケじゃないのよ。
　ただ…… 」
> CONTEXT: Map005/events/1/pages/4/17/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「いいんだよ。それよりほら、見て！
　この写真、すごく良く撮れてる。昔の\\N[1]ちゃんだよ 」
> CONTEXT: Map005/events/1/pages/4/21/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あ！ それ！ モリィが失くした写真かしら！？
　どこにあったの！？ 」
> CONTEXT: Map005/events/1/pages/4/27/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/133/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「モリィのだったのか。どうりで良く撮れてると思ったよ。
　そこで拾ったんだけど……これは返さないといけないね 」
> CONTEXT: Map005/events/1/pages/4/31/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/133/13/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「私が返しておくわ。ちょうどモリィに探して欲しいって
　言われてたの」
> CONTEXT: Map005/events/1/pages/4/36/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/133/18/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「そっか。任せるよ 」
> CONTEXT: Map005/events/1/pages/4/40/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/133/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「みつけてくれてありがとう、助かったわ\\I[122] 」
> CONTEXT: Map005/events/1/pages/4/45/Dialogue < UNTRANSLATED
> CONTEXT: Commonevents/133/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「僕ももっと頑張って、\\N[1]ちゃんに
　ふさわしい男になるよ」
> CONTEXT: Map005/events/1/pages/5/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ロイドは 今のままでも十分なのに」
> CONTEXT: Map005/events/1/pages/5/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ロイド
\\C[0]「そ、それにしても\\N[1]ちゃん……その格好は…… 」
> CONTEXT: Map005/events/1/pages/7/29/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/1/pages/7/44/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「う、うん……そうよね……わかってるわ…… 」
> CONTEXT: Map005/events/1/pages/7/32/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/1/pages/7/47/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「ここへ来たって、\\N[1]様の食事はありませんよ？
　一般的な吸血鬼は血液、\\N[1]様は精液、ここは
　オーク達の食事を用意する場所です」
> CONTEXT: Map005/events/2/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……そんなにハッキリ言わないでよ」
> CONTEXT: Map005/events/2/pages/0/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「最近ねずみが巣を作って困ってるのですよ。物置よりは
　マシですけどね。あっちはでっかい穴が開いちゃって、まったく」
> CONTEXT: Map005/events/2/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「へぇ…… 」
> CONTEXT: Map005/events/2/pages/1/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「そこの棚の後ろにも 穴があるみたいなんだけど
　僕じゃ重くて動かせないんだ」
> CONTEXT: Map005/events/2/pages/1/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「誰か\\C[2]力持ち\\C[0]、いないかなぁ？
　居たら、ここに来るように伝えてくれないかい？ 」
> CONTEXT: Map005/events/2/pages/1/16/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「うん、わかった」
> CONTEXT: Map005/events/2/pages/1/21/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[2]クエスト１　コックの悩み\\C[0]
受注しました。
> CONTEXT: Map005/events/2/pages/1/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「物置の穴は、結構ヤバイらしいですけどね」
> CONTEXT: Map005/events/2/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そう。なるべく急ぐわね」
> CONTEXT: Map005/events/2/pages/2/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「いや～ 助かったよ」
> CONTEXT: Map005/events/2/pages/3/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「\\N[1]がどんな選択をされるか、楽しみです！ 」
> CONTEXT: Map005/events/2/pages/5/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「裸で厨房に入っちゃ危ないですよ！！ 」
> CONTEXT: Map005/events/2/pages/6/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「あーあー、そんな格好してぇ！ ヴィクター様に
　叱られてしまいますよ！ 」
> CONTEXT: Map005/events/2/pages/6/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「僕、フェリシアさんのお式の料理を頼まれてるのですよ。
　夢は\\N[1]様のお式の料理を作る事ですけど…… 」
> CONTEXT: Map005/events/2/pages/6/25/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「式には遠い一族も大勢来られるとの事、いや～
　料理にも気合が入りますよ！ 」
> CONTEXT: Map005/events/2/pages/6/34/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「うん、よろしくね！ 」
> CONTEXT: Map005/events/2/pages/6/39/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「お、おぢょう様……、ご、ごきげんうる……うるあしい……？？ 」
> CONTEXT: Map005/events/3/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「無理して綺麗な言葉使おうとしなくていいのよ」
> CONTEXT: Map005/events/3/pages/0/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「で……でもオーク、吸血鬼様一族のドレイ……ちゃんとしないと
　ヴィクター様に、叱られる…… 」
> CONTEXT: Map005/events/3/pages/0/11/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃんはちょっとお堅いのよね～。気にしなくていいわよ」
> CONTEXT: Map005/events/3/pages/0/16/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]様優しい……、でもヴィクター様も
　本当は優しい…… 」
> CONTEXT: Map005/events/3/pages/0/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ふぅ～ん…… 」
> CONTEXT: Map005/events/3/pages/0/23/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]様コンヤク、おめでたい……
　ロイド様も良いお方…… 」
> CONTEXT: Map005/events/3/pages/1/3/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/3/pages/4/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「まぁそうなんだけどね…… 」
> CONTEXT: Map005/events/3/pages/1/8/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/3/pages/4/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ダグって……力はありそうよね…… 」
> CONTEXT: Map005/events/3/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]様、ナニカ、力仕事あるのか……？
　おれ、\\N[1]様のためなら、何でも働く…… 」
> CONTEXT: Map005/events/3/pages/2/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「私っていうか、厨房でコックさんが困ってるのよね。
　ちょっと一緒に来てくれる？ 」
> CONTEXT: Map005/events/3/pages/2/11/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「終わった…… 」
> CONTEXT: Map005/events/3/pages/2/36/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「助かるわ。ありがとう\\I[122] 」
> CONTEXT: Map005/events/3/pages/2/40/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「お安いご用……！ 」
> CONTEXT: Map005/events/3/pages/2/44/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]コック
\\C[0]「２人共ありがとう！ お礼にこれをどうぞ！ 」
> CONTEXT: Map005/events/3/pages/2/50/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「おれにもくれる、う、う、嬉しい…… 」
> CONTEXT: Map005/events/3/pages/2/61/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　　クエスト１　達成！
> CONTEXT: Map005/events/3/pages/2/67/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「コックの飯……美味しい……アイツ料理上手
　\\N[1]様、普通の食事できない？ 可哀想…… 」
> CONTEXT: Map005/events/3/pages/3/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]さま、城に戻ったからって、開放的すぎる…… 」
> CONTEXT: Map005/events/3/pages/6/4/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/3/pages/7/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]さま、そんな格好してるとヴィクター様に
　怒られる…… 」
> CONTEXT: Map005/events/3/pages/6/15/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/3/pages/7/15/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そ、そうかもね…… 」
> CONTEXT: Map005/events/3/pages/6/19/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/3/pages/7/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]さま、誰を選んでも幸せになって欲しい…… 」
> CONTEXT: Map005/events/3/pages/6/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]さまとヴィクター様、お似合い。
　結婚おめでたい…… 」
> CONTEXT: Map005/events/3/pages/7/28/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ありがとう。でも式はまだまだ先よ」
> CONTEXT: Map005/events/3/pages/7/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「ロイドさま、子供の頃から\\N[1]さま、ひとすじ……
　やっと結ばれて良かった」
> CONTEXT: Map005/events/3/pages/7/40/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「種族を超えた愛……ちょっと憧れる。
　オーク族にもまれにある話ではある…… 」
> CONTEXT: Map005/events/3/pages/7/49/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「へー、そうなんだぁ！ ちょっと親近感のある話ね」
> CONTEXT: Map005/events/3/pages/7/54/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ダグ
\\C[0]「\\N[1]さまの食事、精液……こうなるのも
　不思議じゃない……けど残念」
> CONTEXT: Map005/events/3/pages/7/62/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃんが、裏庭を立ち入り禁止にしたのよね……
　子供の頃は よく遊んだのにっ 」
> CONTEXT: Map005/events/5/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]謎の魔女
\\C[0]「うふふ……入り口は開かないみたいね。
　地下からスルスルローションで出たら、また ここまで歩き直しよ」
> CONTEXT: Map005/events/8/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]謎の魔女
\\C[0]「町からここに戻ってこられるアイテムがあるけど……買う？ 」
> CONTEXT: Map005/events/8/pages/0/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]謎の魔女
\\C[0]「毎度ありぃ\\I[122] でもソレごく短い期間しか使えないわよ\\I[122]」
> CONTEXT: Map005/events/8/pages/0/14/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]謎の魔女
\\C[0]「なによ～、買わないのぉ？ 」
> CONTEXT: Map005/events/8/pages/0/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]謎の魔女
\\C[0]「ふ～ん、あっそ！ スルスルローションで地下出たら
　また ここまで歩き直しよ？ 」
> CONTEXT: Map005/events/8/pages/0/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「へっへっへ、お嬢様……暗い城内に閉じ篭りっきりじゃ
　退屈でしょう？ このファルサーめが、兄上様に内緒で
　お相手して差し上げましょうか？ 」
> CONTEXT: Map005/events/9/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「気持ち悪いわ……アンタって笑顔が悪魔みたいよね」
> CONTEXT: Map005/events/9/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「失礼な。私は誠心誠意、この城に使える忠実な家来ですぞ」
> CONTEXT: Map005/events/9/pages/0/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「それはわかってるけど～…… 」
> CONTEXT: Map005/events/9/pages/0/16/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「何かありましたら、いつでも頼って下され。
　何でもお力になりますよ」
> CONTEXT: Map005/events/9/pages/0/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そ～お？ ありがとうね」
> CONTEXT: Map005/events/9/pages/0/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「ぐふふ、お嬢様、婚約されたのですってね。とは言え本心は
　おめでたくはないでしょう？ 」
> CONTEXT: Map005/events/9/pages/1/3/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/9/pages/2/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「その話はやめてよ…… 」
> CONTEXT: Map005/events/9/pages/1/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「その話はやめてよ……
　それより物置を見たいんだけどな？ 」
> CONTEXT: Map005/events/9/pages/2/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「\\N[1]様のお望みなら、なんだってお聞きしますよ
　しかしあいにく、鍵を失くしてしまいまして…… 」
> CONTEXT: Map005/events/9/pages/2/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「確か\\C[2]合鍵\\C[0]が、兄上様のお部屋にあるはずですが……
　私めがおいそれとは入れませんので…… 」
> CONTEXT: Map005/events/9/pages/2/17/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「私が取ってくればいいのね！ 任せて！ 」
> CONTEXT: Map005/events/9/pages/2/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[2]クエスト３　ファルサーの頼み\\C[0]
受注しました。
> CONTEXT: Map005/events/9/pages/2/28/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「しかし、お嬢様がロイドごときと婚約されるとはね…… 」
> CONTEXT: Map005/events/9/pages/3/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「なによ、その言い方？ ロイドに失礼でしょ！ 」
> CONTEXT: Map005/events/9/pages/3/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「\\N[1]様にとっては、兄上様の足元にも
　及ばないでしょう？ 」
> CONTEXT: Map005/events/9/pages/3/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「…………合鍵、取ってくるわ…… 」
> CONTEXT: Map005/events/9/pages/3/15/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]ファルサー
\\C[0]「おぉ！ さすがは\\N[1]様！ これで中をお見せできます。
　ありがとうございます」
> CONTEXT: Map005/events/9/pages/4/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　　クエスト３　達成！
> CONTEXT: Map005/events/9/pages/4/12/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃんって用心深いわね…… 」
> CONTEXT: Map005/events/10/pages/0/9/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/30/pages/0/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「これじゃ出られそうにないわね。
　他に出口が合っても良さそうだけど…… 」
> CONTEXT: Map005/events/10/pages/1/9/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/30/pages/1/9/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
調理器具が並んでいる……
> CONTEXT: Map005/events/13/pages/0/4/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/16/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「どうやって使うのかしら…… 」
> CONTEXT: Map005/events/13/pages/0/6/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/16/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「へ～、ネズミも頑張ってるのね……」
> CONTEXT: Map005/events/14/pages/1/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「物置の穴って、どんなのかしら……」
> CONTEXT: Map005/events/14/pages/1/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
中は物で溢れていそうな気配がする……
> CONTEXT: Map005/events/22/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ここは開けちゃいけない気がするわ…… 」
> CONTEXT: Map005/events/22/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
乱雑に物がしまわれている……
> CONTEXT: Map005/events/23/pages/0/4/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/24/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「オーク達ってガサツね…… 」
> CONTEXT: Map005/events/23/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「整理整頓しないのね…… 」
> CONTEXT: Map005/events/24/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
武器が置いてある……
> CONTEXT: Map005/events/25/pages/0/3/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/26/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「お兄ちゃん……城を守るために備えてるけど
　使った試しがないのよね…… 」
> CONTEXT: Map005/events/25/pages/0/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「防衛用の武器か……いつ使うのかしら…… 」
> CONTEXT: Map005/events/26/pages/0/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
キレイな食器が並んでいる……
> CONTEXT: Map005/events/27/pages/0/4/Dialogue < UNTRANSLATED
> CONTEXT: Map005/events/28/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「私にはあんまり関係ないのよね…… 」
> CONTEXT: Map005/events/27/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「食器で食事っていうのも、憧れるな～…… 」
> CONTEXT: Map005/events/28/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
ワインセラー……？
> CONTEXT: Map005/events/29/pages/0/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「中身はあんまり知りたくないかも…… 」
> CONTEXT: Map005/events/29/pages/0/6/Dialogue < UNTRANSLATED

> END STRING
