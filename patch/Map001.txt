> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
ワールドマップ
> CONTEXT: Map001/display_name/ < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そろそろお城の様子でも見に行こうかな～…… 」
> CONTEXT: Map001/events/6/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「そうですねぇ、そろそろ意地を張るのをやめて、お兄様に
　ひとこと謝ってもよろしいかと…… 」
> CONTEXT: Map001/events/6/pages/0/8/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「うっ……！ た、確かに私も色々悪かったかもしれないわよ？
　色々忘れてた事も思い出したしね」
> CONTEXT: Map001/events/6/pages/0/13/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「でも、お兄ちゃん婚約してるのよ！？ 散々私と寝ておいて！
　フェリシアさんと！ ……そりゃあ、フェリシアさんはいい人だし
　お兄ちゃんの相手として不服とかじゃないけど…… 」
> CONTEXT: Map001/events/6/pages/0/19/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「あわわわっ！ \\N[1]様の居場所がなくなるわけでは
　ありませんよ！ こうしてモリィもありますし、ロイド様だって
　お優しい方で、夫としては理想かもしれませんし！ 」
> CONTEXT: Map001/events/6/pages/0/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうね……私がワガママ言わなければ、みんなが
　丸く収まるのかも…… 」
> CONTEXT: Map001/events/6/pages/0/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「私ね、お城を抜け出して色々な場所に行って、昔の事
　結構思い出したの…… 」
> CONTEXT: Map001/events/6/pages/0/42/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「モリィもお兄ちゃんも、ロイドも、みんな私の事
　大事にしてくれてたなって……。１つ思い通りに
　ならないからって 勝手だったかな…… 」
> CONTEXT: Map001/events/6/pages/0/47/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「私は\\N[1]様が何をしようとも、全てを敬愛致します！ 」
> CONTEXT: Map001/events/6/pages/0/53/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ふふ、ありがとうモリィ。ロイドもきっとそうね。
　時々傍で見てて助けてくれてるみたいだし…… 」
> CONTEXT: Map001/events/6/pages/0/57/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「まだ帰るって決めたわけじゃないけど、様子を見に行こうかな？ 」
> CONTEXT: Map001/events/6/pages/0/64/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「でもこっちに出てきた時の道ってファルサーが
　塞いじゃってるのよね。みんなどうやって来てるのかしら？ 」
> CONTEXT: Map001/events/6/pages/0/67/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「\\C[2]別のルート\\C[0]があるのかもしれませんねぇ」
> CONTEXT: Map001/events/6/pages/0/72/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうね、何か\\C[2]情報\\C[0]があるといいんだけど…… 」
> CONTEXT: Map001/events/6/pages/0/75/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「道がわかったら一旦お城に戻るわ！ ね、モリィも
　疲れてるでしょ？ 」
> CONTEXT: Map001/events/6/pages/0/79/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「私は\\N[1]様とご一緒できれば、火の中だろうと
　どこでも快適でございます！ 」
> CONTEXT: Map001/events/6/pages/0/84/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[2]クエスト４０　城の様子を…\\C[0]
解禁しました。
> CONTEXT: Map001/events/6/pages/0/92/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　クエスト４１　達成！
> CONTEXT: Map001/events/7/pages/0/5/Dialogue < UNTRANSLATED

> END STRING
