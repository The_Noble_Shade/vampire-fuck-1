> RPGMAKER TRANS PATCH FILE VERSION 3.2
> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「ひゃ～、そんな格好で話しかけないで下さい！！ 」
> CONTEXT: Map003/events/3/pages/0/5/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/3/pages/0/15/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/3/pages/1/5/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/3/pages/1/14/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/3/pages/2/5/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/3/pages/2/15/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「私は旅の吸血鬼。道具屋をしながら暮らしております。
　ここのお城では門前払いでしたけど…… 」
> CONTEXT: Map003/events/3/pages/0/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「あら、そうなの。ごめんなさいね。
　だったら他の場所へ行った方がいいんじゃない……？ 」
> CONTEXT: Map003/events/3/pages/0/29/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「実は……、\\C[2]売り物\\C[0]をごっそり盗まれてしまいました。
　一部は残っていますが、これでは商売になりません…… 」
> CONTEXT: Map003/events/3/pages/0/34/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「犯人はこの城の庭に潜んでいるようですが、なにぶん
　私は非力な吸血鬼でして、取り戻せずに困っています」
> CONTEXT: Map003/events/3/pages/0/38/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「誰かが\\C[2]売り物\\C[0]を取り返してきてくれれば
　商売ができるのですが…… 」
> CONTEXT: Map003/events/3/pages/0/42/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「なるほどぉ、困ってるみたいね。どうしようかなぁ～…… 」
> CONTEXT: Map003/events/3/pages/0/48/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
放っておく
> CONTEXT: Map003/events/3/pages/0/51/Choice/1 < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「よぉ～し、この\\N[1]様に任せなさい！
　\\C[2]売り物\\C[0]を取り返してきちゃうんだからっ\\I[122] 」
> CONTEXT: Map003/events/3/pages/0/54/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「お願いします！ 取り戻して頂ければ、色々とお役に
　立てるはずです！ 」
> CONTEXT: Map003/events/3/pages/0/58/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[2]クエスト４　商売できません\\C[0]
受注しました。
> CONTEXT: Map003/events/3/pages/0/66/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ごめんね、協力できそうにないわ」
> CONTEXT: Map003/events/3/pages/0/72/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「そうですか、気が変わったらお願いします」
> CONTEXT: Map003/events/3/pages/0/75/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「よろしくお願いします。あなただけが頼りです 」
> CONTEXT: Map003/events/3/pages/1/22/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「うふっ、任せて！ 」
> CONTEXT: Map003/events/3/pages/1/26/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「売り物、取り返してきたわ」
> CONTEXT: Map003/events/3/pages/2/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「あっ、ありがとうございます！ これで商売ができますぅぅ！！ 」
> CONTEXT: Map003/events/3/pages/2/28/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「さぁ、準備ができました。何かご用の際には
　声をかけて下さいね！ 」
> CONTEXT: Map003/events/3/pages/2/38/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「そうね、買い物があったら、させてもらうわ」
> CONTEXT: Map003/events/3/pages/2/42/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING

　　　　　　　　　　　　　クエスト４　達成！
> CONTEXT: Map003/events/3/pages/2/49/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「いらっしゃいませ 」
> CONTEXT: Map003/events/3/pages/3/5/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/3/pages/3/19/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/3/pages/3/32/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「すみませんが、服は売っていません…… 」
> CONTEXT: Map003/events/3/pages/3/10/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]旅の吸血鬼
\\C[0]「それにしても、すごい格好ですね…… 」
> CONTEXT: Map003/events/3/pages/3/24/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
出てきた時の穴はふさがっている……
> CONTEXT: Map003/events/4/pages/0/0/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/4/pages/1/0/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「完全に塞がっちゃってる……出てきた時
　結構 無理したしなぁ…… 」
> CONTEXT: Map003/events/4/pages/0/5/Dialogue < UNTRANSLATED
> CONTEXT: Map003/events/4/pages/1/5/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「このお城ともお別れね……ちょっと寂しいけど」
> CONTEXT: Map003/events/6/pages/0/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ファルサー……！ 絶対許さないんだからっ！ 」
> CONTEXT: Map003/events/6/pages/1/6/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ファルサー……！ 今度こそ、絶対許さないんだからっ！ 」
> CONTEXT: Map003/events/6/pages/2/4/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「うっ……ちょっと狭い…… 」
> CONTEXT: Map003/events/9/pages/0/3/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「わっ……！ 」
> CONTEXT: Map003/events/9/pages/0/7/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「ふぅ……やっと外に出たわね。魔物か……ちょっと不安だな」
> CONTEXT: Map003/events/9/pages/0/18/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「大丈夫ですよ！ もし負けても、その時は私がお助け致します！ 」
> CONTEXT: Map003/events/9/pages/0/21/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「それは有難いんだけど……お兄ちゃん以外とエッチって
　まだ慣れないから……その…… 」
> CONTEXT: Map003/events/9/pages/0/27/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「それは有難いんだけど……お兄ちゃん以外とエッチって
　初めてだから……その…… 」
> CONTEXT: Map003/events/9/pages/0/33/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「\\N[1]様にも羞恥心がおありとは……散々私の前で
　あんな事やこんな事を なさってましたけど…… 」
> CONTEXT: Map003/events/9/pages/0/41/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「……それもそうね。今後の人生の発展のために
　そこは開き直るわ！ どうせ相手は魔物だしっ、数になんか
　入らないんだからっ！ 」
> CONTEXT: Map003/events/9/pages/0/47/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「はぁ～、それにしても お外の空気っておいしい\\I[122]
　さっそく墓地に向かいましょうか」
> CONTEXT: Map003/events/9/pages/0/54/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「おっと、不覚にも忘れる所でした！ これを！ 」
> CONTEXT: Map003/events/9/pages/0/63/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「なにこれ……？ デビルウィング？ 」
> CONTEXT: Map003/events/9/pages/0/70/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]モリィ
\\C[0]「取り急ぎではありますが、私がこしらえました。
　これを装備すれば、敵と戦わずとも済みます画期的なアイテムで
　私の一族に伝わる魔術をベースにですね…… 」
> CONTEXT: Map003/events/9/pages/0/74/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
\\C[1]\\N[1]
\\C[0]「うん、わかった。難しい説明はいいわ（長くなりそうだから）。
　ありがとう、モリィ\\I[122] 」
> CONTEXT: Map003/events/9/pages/0/80/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
袋の中は空のようだ……
> CONTEXT: Map003/events/11/pages/0/0/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
袋の中は 売り物でいっぱいだ。
> CONTEXT: Map003/events/11/pages/1/0/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
空箱が並んでいる……
> CONTEXT: Map003/events/12/pages/0/0/Dialogue < UNTRANSLATED

> END STRING

> BEGIN STRING
ポーション類が陳列されている。
> CONTEXT: Map003/events/12/pages/1/0/Dialogue < UNTRANSLATED

> END STRING
